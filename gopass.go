package main

import (
	"fmt"
	"html/template"
	"math/rand"
	"net/http"
	"time"

	"github.com/tredoe/osutil/user/crypt/sha512_crypt"
)

func main() {

	http.HandleFunc("/", passHandler)
	http.ListenAndServe(":8080", nil)
}

func passHandler(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "POST":
		c := sha512_crypt.New()
		hash, _ := c.Generate([]byte(r.PostFormValue("password")), []byte(fmt.Sprintf("$6$%v", RandStringRunes(7))))
		w.Write([]byte(hash))
	case "GET":
		t, _ := template.ParseFiles("view.tmpl")
		t.Execute(w, nil)
	}
}
// Stolen Code!
func init() {
	rand.Seed(time.Now().UnixNano())
}

var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

func RandStringRunes(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}
